// The Configuration decided by K.sasaki when July 2019

module.exports = {
  'env': {
    'browser': true,
    'es6': true
  },
  'extends': 'eslint:recommended',
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly'
  },
  'parser': 'babel-eslint',
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true
    },
    'ecmaVersion': 2018,
    'sourceType': 'module'
  },
  'plugins': [
    'react',
    'react-native'
  ],
  'rules': {
    'react/jsx-uses-vars': 'error',
    'react/jsx-uses-react': 'error',
    'indent': [
        'error',
        2
    ],
    'linebreak-style': [
      'error',
      'unix'
    ],
    'quotes': [
      'error',
      'single'
    ],
    'semi': [
      'error',
      'always'
    ],
    'space-before-blocks': [
      'error',
      {
        'functions': 'always',
        'keywords': 'always',
        'classes': 'always'
      },
    ],
    'comma-dangle': [
      'error',
      'never'
    ],
    'comma-style': [
      'error',
      'last'
    ],
    'comma-spacing': [
      'error',
      {
        'after': true
      }
    ],
    'camelcase': [
      'error',
      {
        'properties': 'always'
      }
    ],
    'no-unused-vars': [
      'error'
    ],
    'object-curly-spacing': [
      'error',
      'always'
    ],
    'key-spacing': [
      'error',
      {
        'beforeColon': false,
        'afterColon': true,
        'align': 'colon'
      }
    ],
    'no-tabs': 'error',
    'space-before-function-paren': [
      'error',
      'never'
    ],
    'block-spacing': [
      'error',
      'always'
    ],
    'no-trailing-spaces': [
      'error',
      { 
        "skipBlankLines": true 
      }
    ],
    'arrow-parens': [
      'error',
      'always'
    ]

  }
};
