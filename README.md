# HOW TO USE

## 準備

以下実行前にこのプロジェクトのあるディレクトリにいることを確認してからコマンドを実行すること．



### npm パッケージのインストール
```
$ npm install --save-dev
```

### API ホストの設定
TBA

---

## 実行

### iOS エミュレータ
```
$ expo start --ios
```

### Android エミュレータ
```
$ expo start --android
```



### 実機
 `lan`  もしくは `tunnel` を選んで実行．
```
$ expo start --lan
$ expo start --tunnel
```
* `lan` モードで実行するとホスト(上記コマンドを叩いたPC)と同じLANに接続されているデバイスであれば接続が可能．
* `tunnel` モードの場合は外部ネットワークからの接続が可能．ただし `lan` に比べて速度は劣る．


注）APIサーバーをlocalに建てている場合 `tunnel`では接続できない．

---

## コードの整形

コーディング規約は `ESLint` で取り決めてある．`Atom` や `VSCode` のプラグインを用いて整形するか、下記コマンドを叩く．
```
// hoge.js を整形したい場合
$ npx eslint hoge.js --fix
```